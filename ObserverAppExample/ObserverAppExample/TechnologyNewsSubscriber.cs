﻿using System;

namespace ObserverAppExample
{
    class TechnologyNewsSubscriber : ISubscriber
    {
        private readonly TechnologyNewsPublisher _publisher;
        public TechnologyNewsSubscriber(TechnologyNewsPublisher publisher)
        {
            _publisher = publisher;    
        }
        public void Update()
        {
            decimal lastPrice = _publisher.GetLastAsusZenPrice();
            Console.WriteLine($"Tech Subscriber {this.GetHashCode()} updated last price: {lastPrice}");
        }
    }
}
