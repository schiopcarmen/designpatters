﻿using System.Collections.Generic;

namespace ObserverAppExample
{
    abstract class NewsPublisher
    {
        protected List<ISubscriber> _subscribers = new List<ISubscriber>();
        public void AddSubscriber(ISubscriber subscriber)
        {
            //can verify if subscriber already exists 
            _subscribers.Add(subscriber);
        }

        public void RemoveSubscriber(ISubscriber subscriber)
        {
            //verify if subscriber exists in the list
            _subscribers.Remove(subscriber);
        }

        public void NotifySubscribers()
        {
            foreach (var subscriber in _subscribers)
                subscriber.Update();
        }
    }
}
