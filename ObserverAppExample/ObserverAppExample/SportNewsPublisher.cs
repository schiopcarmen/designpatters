﻿namespace ObserverAppExample
{
    class SportNewsPublisher : NewsPublisher
    {
        private string _lastSportInfo;
        public void SetLastSportInfo(string info)
        {
            _lastSportInfo = info;
            NotifySubscribers();
        }

        public string GetLastSportInfo()
        {
            return _lastSportInfo;
        }
    }
}
