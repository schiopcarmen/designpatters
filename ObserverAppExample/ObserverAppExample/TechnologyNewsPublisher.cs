﻿namespace ObserverAppExample
{
    class TechnologyNewsPublisher : NewsPublisher 
    {
        private decimal _asusZenScreenPrice;

        public void SetLastAsusZenPrice(decimal price)
        {
            _asusZenScreenPrice = price;
            NotifySubscribers();
        }

        public decimal GetLastAsusZenPrice()
        {
            return _asusZenScreenPrice;
        }
    }
}
