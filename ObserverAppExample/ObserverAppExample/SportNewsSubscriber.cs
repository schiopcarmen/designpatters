﻿using System;

namespace ObserverAppExample
{
    class SportNewsSubscriber : ISubscriber
    {
        private readonly SportNewsPublisher _publisher;
        public SportNewsSubscriber(SportNewsPublisher publisher)
        {
            _publisher = publisher;
            //* _publisher.AddSubscriber(this);
        }

        public void Update()
        {
            string info = _publisher.GetLastSportInfo();
            Console.WriteLine($"Sport Subscriber {this.GetHashCode()} updated info: {info}");
        }
    }
}
