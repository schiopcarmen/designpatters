﻿using System;

namespace ObserverAppExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Observer Pattern:");
            SportNewsPublisher sportPublisher = new SportNewsPublisher();

            SportNewsSubscriber sportSubscriber1 = new SportNewsSubscriber(sportPublisher);
            SportNewsSubscriber sportSubscriber2 = new SportNewsSubscriber(sportPublisher);

            //*
            sportPublisher.AddSubscriber(sportSubscriber1);
            sportPublisher.AddSubscriber(sportSubscriber2);

            sportPublisher.SetLastSportInfo("Sport news 1");

            TechnologyNewsPublisher techPublisher = new TechnologyNewsPublisher();

            TechnologyNewsSubscriber techSubscriber1 = new TechnologyNewsSubscriber(techPublisher);
            TechnologyNewsSubscriber techSubscriber2 = new TechnologyNewsSubscriber(techPublisher);
            TechnologyNewsSubscriber techSubscriber3 = new TechnologyNewsSubscriber(techPublisher);

            techPublisher.AddSubscriber(techSubscriber1);
            techPublisher.AddSubscriber(techSubscriber2);
            techPublisher.AddSubscriber(techSubscriber3);

            techPublisher.SetLastAsusZenPrice(1500);
            techPublisher.SetLastAsusZenPrice(2000);
        }
    }
}
