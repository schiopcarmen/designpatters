﻿namespace ObserverAppExample
{
    interface ISubscriber
    {
        void Update();
    }
}
